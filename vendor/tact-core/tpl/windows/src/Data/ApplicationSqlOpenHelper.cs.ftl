<@header?interpret />
using ${project_namespace}.Data.Base;

namespace ${project_namespace}.Data
{
    public class ${project_name?cap_first}SqlOpenHelper : ${project_name?cap_first}SqlOpenHelperBase
    {
        // Pass the connection string to the base class.
        public ${project_name?cap_first}SqlOpenHelper()
        {

        }
    }
}